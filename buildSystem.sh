#!/bin/bash
set -e

settings_vars()
{
    export PROJECT_NAME=$(cat Cargo.toml | sed -n -e 's/^.*name = "/"/p' | sed 's/\"//g' | sed 's/-/_/g' )
    export TEST_PROJECT="GodotProject"
    export EXTENTION_PATH="${TEST_PROJECT}/addons/${PROJECT_NAME}/"
    export GDEXT_PATH="${EXTENTION_PATH}/${PROJECT_NAME}.gdextension"
}

main()
{
    compile
    generate_gdext_file
}

compile()
{
    export CARGO_CMD="cargo ${CARGO_NIGHTLY} build ${BUILD_ADDS} --target ${PLATFORM_LABEL} ${CARGO_RELEASE_ARGS} ${CARGO_DEFAULT_FEATURES} ${CARGO_FEATURES}"
    echo ${CARGO_CMD}
    ${CARGO_CMD}
    mkdir -p ${EXTENTION_PATH}
    cp ${TARGET_PATH} ${EXTENTION_PATH}/${EXTENSION_FILE}
    echo built "${EXTENTION_PATH}/${EXTENSION_FILE}"
}

generate_gdext_file()
{
cat << EOF > ${GDEXT_PATH}
[configuration]
entry_symbol = "gdext_rust_init"
compatibility_minimum = 4.1

[libraries]
linux.debug.arm64 = "lib${PROJECT_NAME}.linux.aarch64.so"
linux.release.arm64 = "lib${PROJECT_NAME}.linux.aarch64.so"
linux.debug.x86_64 = "lib${PROJECT_NAME}.linux.x86_64.so"
linux.release.x86_64 = "lib${PROJECT_NAME}.linux.x86_64.so"
windows.debug.x86_64 = "${PROJECT_NAME}.windows.x86_64.dll"
windows.release.x86_64 = "${PROJECT_NAME}.windows.x86_64.dll"
android.debug.arm64 = "lib${PROJECT_NAME}.android.aarch64.so"
android.release.arm64 = "lib${PROJECT_NAME}.android.aarch64.so"
android.debug.x86_64 = "lib${PROJECT_NAME}.android.x86_64.so"
android.release.x86_64 = "lib${PROJECT_NAME}.android.x86_64.so"
web.debug.wasm32 = "${PROJECT_NAME}.wasm"
web.release.wasm32 = "${PROJECT_NAME}.wasm"
EOF
}

echoVars()
{
    echo PLATFORM_LABEL = ${PLATFORM_LABEL}
    echo CARGO_RELEASE_ARGS = ${CARGO_RELEASE_ARGS}
    echo TARGET_PATH = ${TARGET_PATH}
    echo CARGO_DEFAULT_FEATURES = ${CARGO_DEFAULT_FEATURES}
    echo CARGO_FEATURES = ${CARGO_FEATURES}
    echo CLANG_PATH = ${CLANG_PATH}
    echo CARGO_TARGET_AARCH64_LINUX_ANDROID_LINKER = ${CARGO_TARGET_AARCH64_LINUX_ANDROID_LINKER}
    echo CARGO_TARGET_X86_64_LINUX_ANDROID_LINKER = ${CARGO_TARGET_X86_64_LINUX_ANDROID_LINKER}
    echo CC = ${CC}
    echo PLATFORM_LABEL = ${PLATFORM_LABEL}
    echo TARGET_PATH = ${TARGET_PATH}
    echo EXTENSION_FILE = ${EXTENSION_FILE}
}

envVars()
{

    if [[ $DEBUG = "true" ]]; then
        debugEnv
    else
        releaseEnv
    fi

    if [[ $OS = Windows_NT ]]; then
        windowsEnv
    else
        linuxEnv
    fi
}

debugEnv()
{
    export CARGO_RELEASE_ARGS="" #--debug is not supported
    export TARGET_TYPE_PATH="debug"
}

releaseEnv()
{
    export CARGO_RELEASE_ARGS="--release"
    export TARGET_TYPE_PATH="release"
}

windowsEnv()
{
    #Arm Windows isn't supported.
    export PLATFORM_LABEL="${ARCH}-pc-windows-gnu"
    export TARGET_PATH="target/${PLATFORM_LABEL}/${TARGET_TYPE_PATH}/${PROJECT_NAME}.dll"
    export EXTENSION_FILE=${PROJECT_NAME}.${TARGET}.${ARCH}.dll
}

linuxEnv()
{
    export PLATFORM_LABEL="${ARCH}-unknown-linux-gnu"
    export TARGET_PATH="target/${PLATFORM_LABEL}/${TARGET_TYPE_PATH}/lib${PROJECT_NAME}.so"
    export EXTENSION_FILE=lib${PROJECT_NAME}.${TARGET}.${ARCH}.so
}

androidEnv()
{
    if [[ "$ANDROID_SDK" == "" ]]
    then
        export ANDROID_SDK="${HOME}/Projects/.bin/android_sdk/"
    fi
    export CLANG_PATH="${ANDROID_SDK}/ndk/23.2.8568313/toolchains/llvm/prebuilt/linux-x86_64/bin/clang"
    export CARGO_TARGET_AARCH64_LINUX_ANDROID_LINKER="${ANDROID_SDK}/ndk/23.2.8568313/toolchains/llvm/prebuilt/linux-x86_64/bin/aarch64-linux-android31-clang"
    export CARGO_TARGET_X86_64_LINUX_ANDROID_LINKER="${ANDROID_SDK}/ndk/23.2.8568313/toolchains/llvm/prebuilt/linux-x86_64/bin/x86_64-linux-android31-clang"
    export CC="${CLANG_PATH}"
    export PLATFORM_LABEL="${ARCH}-linux-android"
    export TARGET_PATH="target/${PLATFORM_LABEL}/${TARGET_TYPE_PATH}/lib${PROJECT_NAME}.so"
    export EXTENSION_FILE=lib${PROJECT_NAME}.${TARGET}.${ARCH}.so
}

webEnv()
{
    export CARGO_NIGHTLY="+nightly"
    export BUILD_ADDS="-Zbuild-std"
    export PLATFORM_LABEL="wasm32-unknown-emscripten"
    export TARGET_PATH="target/${PLATFORM_LABEL}/${TARGET_TYPE_PATH}/${PROJECT_NAME}.wasm"
    export EXTENSION_FILE=${PROJECT_NAME}.wasm
}

startingArgs()
{
    export ARCH=$(uname -m)

    if [[ $OS = Windows_NT ]]; then
        export TARGET="windows"
    else
        export TARGET="linux"
    fi

    export CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_LINKER=/usr/bin/x86_64-linux-gnu-gcc
    export CARGO_TARGET_AARCH64_UNKNOWN_LINUX_GNU_LINKER=/usr/bin/aarch64-linux-gnu-gcc
    export CARGO_DEFAULT_FEATURES=""
    export CARGO_FEATURES=""
}

args()
{
    debugEnv
    startingArgs

    while [[ $# -gt 0 ]]
    do
    key="$1"
    case $1 in
        -e|--editor)
            args
            compile
            generate_gdext_file            
            editor
            return -1
            ;;
        -d|--debug)
            debugEnv
            ;;
        -r|--release)
            releaseEnv
            ;;
        --android)
            export TARGET="android"
            ;;
        -w|--windows)
            export TARGET="windows"
            export ARCH="x86_64"
            ;;
        -l|--linux)
            export TARGET="linux"
            ;;
        --web)
            export ARCH="wasm"
            export TARGET="web"
            ;;
        -a|--arm)
            export ARCH="aarch64"
            ;;
        -x|--x64)
            export ARCH="x86_64"
            ;;
        -h|--help)
            echoHelp
            return -1
            ;;
        -s|--sleep)
            sleep 1
            ;;
        -f|--feature)
            CARGO_DEFAULT_FEATURES="--no-default-features"
            CARGO_FEATURES="$2,${CARGO_FEATURES}"
            shift
            ;;
        --all)
            args --release --x64 --android && compile
            args --release --arm --android && compile
            args --release --x64 --linux && compile
            args --release --arm --linux && compile
            args --release --windows && compile
            generate_gdext_file
            return -1
            ;;
        --gdext)
            generate_gdext_filez
            return -1
            ;;
        *)
            echo "Arguement Error: $1"
            return -1
    esac
    shift
    done

    case $TARGET in
        windows)
            windowsEnv
            ;;
        linux)
            linuxEnv
            ;;
        android)
            androidEnv
            ;;
        web)
            webEnv
            ;;
        *)
            echo "bad target!"
            return -1
    esac
    if [[ "${CARGO_FEATURES}" != "" ]]
    then
        export CARGO_FEATURES="--features ${CARGO_FEATURES}"
    fi

    echoVars
}

echoHelp()
{
cat << EOF
$0 usage:
-e or --editor
    launch editor for this version
-d or --debug
    set build to debug (default)
-r or --release
    set build to release
-w or --windows
    set build to Windows
-l or --linux
    set build to Linux
-a or --arm
    set build to Arm64
-x or --x64
    set build to amd64
--all
    build all, windows, linux, and linux arm
-h or --help
    print this help
EOF
}

editor()
{
    godot GodotProject/project.godot
}

ORG_PATH=$(pwd)
cd $(dirname $0)
settings_vars
args $@

main
