use gba_alt::{
    gba::GameBoyAdvance,
    prelude::GamepakBuilder,
    sound::AudioInterface,
};
use godot::{
    classes::{image::Format, Image, ImageTexture},
    global::JoyButton,
    prelude::*,
};

use bit::BitIndex;

#[derive(GodotClass)]
#[class(base=Resource)]
pub struct AlternateGBA {
    // pub(crate) keypad: gba_core::KeypadState,
    pub(crate) core: Option<GameBoyAdvance>,
    pub(crate) base: Base<Resource>,
}

#[godot_api]
impl IResource for AlternateGBA {
    fn init(base: Base<Resource>) -> Self {
        Self { core: None, base }
    }
}

struct DummyAudio {}
impl AudioInterface for DummyAudio {
    fn get_sample_rate(&self) -> i32 {
        44100
    }

    fn push_sample(&mut self, _sample: &gba_alt::sound::StereoSample<i16>) {}
}

#[godot_api]
impl AlternateGBA {
    #[func]
    pub fn load_rom_buffer(
        &mut self,
        rom_buffer: PackedByteArray,
        bios: PackedByteArray,
    ) -> Dictionary {
        let mut output = Dictionary::new();
        let p = rom_buffer.as_slice();
        let pack = GamepakBuilder::new().buffer(p).build();
        match pack {
            Ok(pack) => {
                let bios = bios.as_slice().into();
                let gba = GameBoyAdvance::new(bios, pack, Box::from(DummyAudio {}));
                self.core = gba.into();
                output.set("success", "success");
            }
            Err(error) => {
                output.set("error:", format!("{:?}", error));
            }
        }
        output
    }

    fn get_core(&mut self) -> &mut GameBoyAdvance {
        self.core.as_mut().unwrap()
    }

    #[func]
    fn frame(&mut self) {
        self.get_core().frame();
    }

    #[func]
    pub fn cycle(&mut self, _delta: f32, _signals: bool) {
        todo!()
        // self.get_core().frame();

        // const DELTA_SIXTY: f32 = 1. / 60.;
        // self.delta += delta;
        // let mut cycle_count = 0;
        // while self.delta > DELTA_SIXTY {
        //     cycle_count += 1;
        //     self.delta -= DELTA_SIXTY;
        //     match &mut self.core {
        //         Some(c) => {
        //             c.set_keypad_state(self.keypad);
        //             c.emulate_frame(signals);
        //         }
        //         None => {
        //             godot_error!("Cannot cycle before init.")
        //         }
        //     };
        // }
        // cycle_count
    }

    #[func]
    pub fn display(&mut self) -> PackedByteArray {
        let output = match &self.core {
            Some(c) => {
                let a = c.get_frame_buffer();
                let a = a
                    .iter()
                    .map(|f| {
                        let e = f.to_be_bytes();
                        [e[1], e[2], e[3]]
                        // [123,32,255]
                    })
                    .flatten()
                    .collect::<Vec<u8>>();

                PackedByteArray::from(a)
            }
            None => PackedByteArray::new(),
        };
        output
    }

    #[func]
    pub fn screen_texture(&mut self) -> Gd<ImageTexture> {
        let data = self.display();
        if data.len() != 0 {
            let image = Image::create_from_data(240, 160, false, Format::RGB8, &data).unwrap();
            ImageTexture::create_from_image(&image).unwrap()
        } else {
            ImageTexture::new_gd()
        }
    }

    #[func]
    pub fn key_down(&mut self, index: JoyButton) {
        self.set_key(index, true);
    }

    #[func]
    pub fn key_up(&mut self, index: JoyButton) {
        self.set_key(index, false);
    }

    #[func]
    pub fn set_key(&mut self, index: JoyButton, is_down: bool) {
        let key_state = self.get_core().get_key_state_mut();
        let pos = match index {
            JoyButton::A              => 0,
            JoyButton::B              => 1,
            JoyButton::START          => 2,
            JoyButton::BACK           => 3,
            JoyButton::DPAD_LEFT      => 4,
            JoyButton::DPAD_UP        => 5,
            JoyButton::DPAD_RIGHT     => 6,
            JoyButton::DPAD_DOWN      => 7,
            JoyButton::LEFT_SHOULDER  => 8,
            JoyButton::RIGHT_SHOULDER => 9,
            _ => {
                panic!("invalid button!")
            }
        };
        key_state.set_bit(pos, is_down);
    }

    #[func]
    pub fn get_audio_buffer(&self) -> PackedVector2Array {
        todo!()
        // match &self.core {
        //     Some(d) => d
        //         .audio_buffer()
        //         .chunks(2)
        //         .map(|i| {
        //             let a = Vector2::new(
        //                 i[0] as f32 / (i16::MAX as f32),
        //                 i[1] as f32 / (i16::MAX as f32),
        //             );
        //             a
        //         })
        //         .collect::<Vec<Vector2>>()
        //         .into(),
        //     None => PackedVector2Array::new(),
        // }
    }
}
