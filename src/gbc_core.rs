use std::sync::mpsc::{self, Receiver, Sender};

use godot::{
    classes::{image::Format, Image, ImageTexture},
    global::JoyButton,
    prelude::*,
};
use rboy::{device::Device, AudioPlayer, KeypadKey};

/// Godot Boy is a GameBoy emulator built for Godot using RBoy
/// [RBoy](https://github.com/mvdnes/rboy)
/// Template Repository: https://gitlab.com/greenfox/gbc-exe-template
#[derive(GodotClass)]
#[class(base=Resource)]
pub struct GodotBoy {
    audio_pipe_out: Option<Receiver<Vec<(f32, f32)>>>,
    device: Option<Device>,
    base: Base<Resource>,
    owed_cycles: i64,
}

#[godot_api]
impl IResource for GodotBoy {
    fn init(base: Base<Resource>) -> Self {
        Self {
            device: None,
            base,
            audio_pipe_out: None,
            owed_cycles: 0,
        }
    }
}

#[godot_api]
impl GodotBoy {
    /// Loads rom file from a PackedByteArray.
    /// Load the file:
    /// ```gdscript
    /// var gb = GodotBoy.new()
    /// var file = FileAccess.open(r,FileAccess.READ)
    /// var rom = file.get_buffer(file.get_length())
    /// gb.load_rom_buffer(rom)
    /// ```
    /// Returns a dictionary with either `dic["valid"] == true` and a `dic["rom_name"]`
    /// or `dic["valid"] == false` and a `dic["reason"]`
    #[func]
    fn load_rom_buffer(&mut self, rom_buffer: PackedByteArray) -> Dictionary {
        let mut output = Dictionary::new();
        (self.device, self.audio_pipe_out) =
            match Device::new_cgb_from_buffer(rom_buffer.to_vec(), false) {
                Ok(mut d) => {
                    output.set("rom_name", d.romname());
                    output.set("valid", true);

                    let (tx, rx) = mpsc::channel();
                    d.enable_audio(Box::from(GodotBoyAudio::new(tx)));
                    (Option::from(d), Option::from(rx))
                }
                Err(e) => {
                    output.set("valid", false);
                    output.set("reason", e);
                    (None, None)
                }
            };
        output
    }

    /// Loads ram (save data) from a PackedByteArray.
    /// Load like `load_rom_buffer()`.
    #[func]
    fn load_ram_buffer(&mut self, ram_buffer: PackedByteArray) -> Dictionary {
        let mut output = Dictionary::new();
        self.with_device(|d| match d.loadram(ram_buffer.as_slice()) {
            Ok(_) => {
                output.set("loaded", true);
            }
            Err(e) => {
                output.set("error", e.to_godot());
            }
        });
        output
    }

    /// Retrieves ram (save data) buffer.
    #[func]
    fn get_ram_buffer(&mut self) -> PackedByteArray {
        match &mut self.device {
            Some(d) => PackedByteArray::from_iter(d.dumpram().into_iter()),
            None => PackedByteArray::new(),
        }
    }

    /// Runs cycles and emits a signal if the screen updates.
    /// GodotBoy expects 4194304 cycles per second to run at full speed.
    #[func]
    fn cycle(&mut self, count: i64, signals: bool) -> u32 {
        let mut done_cycles = -self.owed_cycles;
        self.with_device(|d| {
            while done_cycles < count {
                done_cycles += d.do_cycle() as i64;
            }
        });
        self.owed_cycles = done_cycles - count;
        if signals {
            self.check_screen_signal();
            self.emit_audio_buffers();
            self.check_ram_signal();
        }
        done_cycles as u32
    }

    fn check_screen_signal(&mut self) {
        let texture = match &mut self.device {
            Some(d) => {
                if d.check_and_reset_gpu_updated() {
                    let t: Gd<ImageTexture> =
                        Self::make_texture_from_data({
                            let g = d.get_gpu_data().into();
                            g
                        }).into();
                    Some(t)
                    // t
                } else {
                    None
                }
            }
            None => None,
        };
        match &texture {
            Some(texture) => {
                self.base_mut()
                    .emit_signal("screen_update", &[texture.to_variant()]);
            }
            None => {}
        };
    }

    fn emit_audio_buffers(&mut self) {
        let mut out = Vec::<PackedVector2Array>::new();
        match &self.audio_pipe_out {
            Some(a) => {
                while let Ok(msg) = a.try_recv() {
                    let a =
                        PackedVector2Array::from_iter(msg.iter().map(|f| Vector2::new(f.0, f.1)));
                    // let a = );
                    out.push(a);
                }
            }
            None => {}
        }
        out.iter().for_each(|a| {
            self.base_mut()
                .emit_signal("push_audio_buffer", &[a.to_variant()]);
        });
    }

    fn check_ram_signal(&mut self) {
        if self.check_and_reset_ram_updated() {
            let buffer = self.get_ram_buffer();
            self.base_mut()
                .emit_signal("ram_update", &[buffer.to_variant()]);
        }
    }

    /// Returns true if the screen was updated. Reset to false if it was true.
    #[func]
    fn check_and_reset_gpu_updated(&mut self) -> bool {
        match &mut self.device {
            Some(d) => d.check_and_reset_gpu_updated(),
            None => false,
        }
    }

    /// Cycles the emulator by delta time (secs).
    /// This will run 4194304 cycles per second.
    #[func]
    fn delta_cycle(&mut self, delta: f32, signals: bool) {
        let cycles = (4194304. * delta) as i64;
        self.cycle(cycles, signals);
    }

    /// Gets raw GPU data. Can be converted into an ImageTexture.
    /// `get_screen_texture()` will likely be easier.
    #[func]
    fn get_gpu_data(&mut self) -> PackedByteArray {
        match &mut self.device {
            Some(device) => device.get_gpu_data().into(),
            None => {
                godot_print!("Device not loaded.");
                PackedByteArray::new()
            }
        }
    }

    fn make_texture_from_data(data: PackedByteArray) -> Gd<ImageTexture> {
        if data.len() != 0 {
            let image = Image::create_from_data(160, 144, false, Format::RGB8, &data).unwrap();
            ImageTexture::create_from_image(&image).unwrap()
        } else {
            ImageTexture::new_gd()
        }
    }

    /// Returns the current screen texture.
    #[func]
    fn get_screen_texture(&mut self) -> Gd<ImageTexture> {
        Self::make_texture_from_data(self.get_gpu_data())
    }

    /// Sets key value.
    #[func]
    fn key_set(&mut self, key: JoyButton, value:bool) {
        if value {
            self.key_up(key);
        } else {
            self.key_down(key);
        }
    }

    /// Sets key up. Expects a JoyButton.
    #[func]
    fn key_down(&mut self, key: JoyButton) {
        let _ = Self::to_key_enum(key).is_ok_and(|k| {
            self.with_device(|d| d.keydown(k));
            true
        });
    }

    /// Sets key down. Expects a JoyButton.
    #[func]
    fn key_up(&mut self, key: JoyButton) {
        let key = Self::to_key_enum(key).unwrap();
        self.with_device(|d| d.keyup(key));
    }

    fn to_key_enum(button: JoyButton) -> Result<KeypadKey, ()> {
        match button {
            JoyButton::DPAD_RIGHT => Ok(KeypadKey::Right),
            JoyButton::DPAD_LEFT => Ok(KeypadKey::Left),
            JoyButton::DPAD_UP => Ok(KeypadKey::Up),
            JoyButton::DPAD_DOWN => Ok(KeypadKey::Down),
            JoyButton::A => Ok(KeypadKey::A),
            JoyButton::B => Ok(KeypadKey::B),
            JoyButton::BACK => Ok(KeypadKey::Select),
            JoyButton::START => Ok(KeypadKey::Start),
            _ => Err(godot_error!("Invalid Key for this device: {:?}", button)),
        }
    }

    /// Returns true when a ROM is properly loaded.
    #[func]
    fn is_loaded(&mut self) -> bool {
        self.device.is_some()
    }

    /// Unloads the rom.
    #[func]
    fn unload(&mut self) {
        self.device = None
    }

    /// Unfinished Prototype.
    #[func]
    fn attach_printer(&mut self) {
        self.with_device(|d| d.attach_printer());
    }

    /// Returns true when the ram (save data) has changed.
    /// Check for this after cycling and read and save data to
    /// keep games from losing data when the emulator shuts down or reboots.
    #[func]
    fn check_and_reset_ram_updated(&mut self) -> bool {
        match &mut self.device {
            Some(d) => d.check_and_reset_ram_updated(),
            None => false,
        }
    }

    fn with_device<F: FnOnce(&mut Device)>(&mut self, callback: F) {
        match &mut self.device {
            Some(d) => callback(d),
            None => {}
        }
    }

    /// Emits a ImageTexture when the screen updates.
    #[signal]
    fn screen_update(data: Gd<ImageTexture>);

    // Emits when PackedByteArray when RAM (save data) updates.
    #[signal]
    fn ram_update(data: PackedByteArray);

    /// Emits a buffer for audio nodes.
    /// Expects an AudioStreamPlayer, AudioStreamPlayer2D, or AudioStreamPlayer3D.
    /// Stream must be set to Generator. Set the stream to Play.
    /// Attach this signal to something like this:
    /// ```GDScript
    /// %speaker.play()
    /// gb.push_audio_buffer.connect(func(data):
    ///     %speaker.get_stream_playback().push_buffer(data)
    /// )
    /// ```
    #[signal]
    fn push_audio_buffer(data: PackedVector2Array);
}

struct GodotBoyAudio {
    pipe_in: Sender<Vec<(f32, f32)>>,
}
impl GodotBoyAudio {
    fn new(pipe_in: Sender<Vec<(f32, f32)>>) -> Self {
        Self { pipe_in }
    }
}

impl AudioPlayer for GodotBoyAudio {
    fn play(&mut self, left_channel: &[f32], right_channel: &[f32]) {
        let g: Vec<(f32, f32)> = left_channel
            .iter()
            .zip(right_channel.iter())
            .map(|(l, r)| (l.clone(), r.clone()))
            .collect();
        self.pipe_in.send(g).unwrap();
    }

    fn samples_rate(&self) -> u32 {
        44100
    }

    fn underflowed(&self) -> bool {
        true
    }
}
