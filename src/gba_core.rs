use gba_core::{Gba, KeypadState, Rom};
use godot::{
    classes::{image::Format, Image, ImageTexture},
    global::JoyButton,
    prelude::*,
};

#[derive(GodotClass)]
#[class(base=Resource)]
pub struct AdvancedGodotBoy {
    pub(crate) keypad: gba_core::KeypadState,
    pub(crate) core: Option<gba_core::Gba>,
    pub(crate) base: Base<Resource>,
    delta: f32,
}

#[godot_api]
impl IResource for AdvancedGodotBoy {
    fn init(base: Base<Resource>) -> Self {
        Self {
            keypad: KeypadState::default(),
            core: None,
            base,
            delta: 0.,
        }
    }
}

#[godot_api]
impl AdvancedGodotBoy {
    #[func]
    pub fn load_rom_buffer(
        &mut self,
        rom_buffer: PackedByteArray,
        bios: PackedByteArray,
    ) -> Dictionary {
        let mut output = Dictionary::new();
        let data = rom_buffer.as_slice();
        let rom = Rom::new(data);
        let bios = bios.as_slice().into();
        let builder = Gba::builder(bios, rom);
        let core = builder.build();
        self.core = Option::from(core);
        output.set("todo", "message load state");
        output
    }

    #[func]
    pub(crate) fn cycle_old(&mut self) {
        match &mut self.core {
            Some(c) => {
                c.set_keypad_state(self.keypad);
                c.emulate_frame(true);
            }
            None => {}
        };
    }

    #[func]
    pub fn cycle(&mut self, delta: f32, signals: bool) -> i32 {
        const DELTA_SIXTY: f32 = 1. / 60.;
        self.delta += delta;
        let mut cycle_count = 0;
        while self.delta > DELTA_SIXTY {
            cycle_count += 1;
            self.delta -= DELTA_SIXTY;
            match &mut self.core {
                Some(c) => {
                    c.set_keypad_state(self.keypad);
                    c.emulate_frame(signals);
                }
                None => {
                    godot_error!("Cannot cycle before init.")
                }
            };
        }
        cycle_count
    }

    // #[func]
    // pub fn cycle(&mut self, delta: f32, signals: bool) {
    //     match &mut self.core {
    //         Some(c) => {
    //             c.should_render = signals;
    //             c.set_keypad_state(self.keypad);
    //             // const cycleSpeed:u32 = (240 + 68) * (160 + 68) * 4;//idk why this equation is like this, but it came from the emulator
    //             const CYCLE_SPEED: f32 = 16853760.0; //16.78 MHz? idk, this value came out of the above equation, multiplied by 60
    //             let cycles = (CYCLE_SPEED * delta) as usize;
    //             c.emulate_cycles(cycles);
    //             // c.emulate_frame(signals);
    //         }
    //         None => {}
    //     };
    // }

    #[func]
    pub fn display(&mut self) -> PackedByteArray {
        let output = match &self.core {
            Some(c) => {
                let a = c.framebuffer();
                let a = a
                    .iter()
                    .map(|f| {
                        let e = f.to_be_bytes();
                        [e[1], e[2], e[3]]
                        // [123,32,255]
                    })
                    .flatten()
                    .collect::<Vec<u8>>();

                PackedByteArray::from(a)
            }
            None => PackedByteArray::new(),
        };
        output
    }

    #[func]
    pub fn screen_texture(&mut self) -> Gd<ImageTexture> {
        let data = self.display();
        if data.len() != 0 {
            let image = Image::create_from_data(240, 160, false, Format::RGB8, &data).unwrap();
            ImageTexture::create_from_image(&image).unwrap()
        } else {
            ImageTexture::new_gd()
        }
    }

    #[func]
    pub fn key_down(&mut self, index: JoyButton) {
        self.set_key(index, true);
    }

    #[func]
    pub fn key_up(&mut self, index: JoyButton) {
        self.set_key(index, false);
    }

    #[func]
    pub fn set_key(&mut self, index: JoyButton, is_down: bool) {
        match index {
            JoyButton::A => self.keypad.a = is_down,
            JoyButton::B => self.keypad.b = is_down,
            JoyButton::START => self.keypad.start = is_down,
            JoyButton::BACK => self.keypad.select = is_down,
            JoyButton::DPAD_UP => self.keypad.up = is_down,
            JoyButton::DPAD_DOWN => self.keypad.down = is_down,
            JoyButton::DPAD_RIGHT => self.keypad.right = is_down,
            JoyButton::DPAD_LEFT => self.keypad.left = is_down,
            JoyButton::LEFT_SHOULDER => self.keypad.l = is_down,
            JoyButton::RIGHT_SHOULDER => self.keypad.r = is_down,
            _ => {}
        }
    }

    #[func]
    pub fn get_audio_buffer(&self) -> PackedVector2Array {
        match &self.core {
            Some(d) => d
                .audio_buffer()
                .chunks(2)
                .map(|i| {
                    let a = Vector2::new(
                        i[0] as f32 / (i16::MAX as f32),
                        i[1] as f32 / (i16::MAX as f32),
                    );
                    a
                })
                .collect::<Vec<Vector2>>()
                .into(),
            None => PackedVector2Array::new(),
        }
    }
}
