use godot::{
    classes::{image::Format, Image, ImageTexture},
    global::JoyButton,
    prelude::*,
};
use nes_rust::Nes;

#[derive(GodotClass)]
#[class(base=Resource)]
pub struct GodotEntertainmentSystem {
    pub(crate) core: Option<Nes>,
    pub(crate) base: Base<Resource>,
}

#[godot_api]
impl IResource for GodotEntertainmentSystem {
    fn init(base: Base<Resource>) -> Self {
        Self { core: None, base }
    }
}

#[godot_api]
impl GodotEntertainmentSystem {
    #[func]
    pub(crate) fn load_rom_buffer(
        &mut self,
        rom_buffer: PackedByteArray,
        bios: PackedByteArray,
    ) -> Dictionary {
        Nes::new(input, display, audio);


        todo!()
    }

    #[func]
    pub(crate) fn cycle(&mut self) {
        todo!()
    }

    #[func]
    pub(crate) fn display(&mut self) -> PackedByteArray {
        todo!()
    }

    #[func]
    fn screen_texture(&mut self) -> Gd<ImageTexture> {
        todo!()
    }

    #[func]
    pub(crate) fn key_down(&mut self, index: JoyButton) {
        self.set_key(index, true);
    }

    #[func]
    pub(crate) fn key_up(&mut self, index: JoyButton) {
        self.set_key(index, false);
    }
    #[func]
    pub(crate) fn set_key(&mut self, index: JoyButton, is_down: bool) {
        todo!()
    }
}
