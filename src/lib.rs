use godot::prelude::*;

struct GodotBoy;

#[gdextension]
unsafe impl ExtensionLibrary for GodotBoy {}

#[cfg(feature = "gbc")]
pub mod gbc_core;

#[cfg(feature = "gba")]
pub mod gba_core;

#[cfg(feature = "nes")]
pub mod nes_core;

#[cfg(feature = "gba_alt")]
pub mod gba_alt;
