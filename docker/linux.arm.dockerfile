FROM rust:1.83

#arm linux
RUN apt-get update -y && apt-get install -y \
  gcc-aarch64-linux-gnu \
  && rm -rf /var/lib/apt/lists/*

RUN rustup target add aarch64-unknown-linux-gnu
