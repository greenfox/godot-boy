FROM rust:1.83

# android
RUN apt-get update -y && apt-get install -y \
  openjdk-17-jdk-headless \
  && rm -rf /var/lib/apt/lists/*

RUN rustup target install \
  aarch64-linux-android \
  x86_64-linux-android

COPY sdk34 /andrdoid_sdk
ENV ANDROID_SDK="/andrdoid_sdk"

#Download Android dev kit, command line tools
# ${SDK_MANAGER} --sdk_root=$(pwd)/sdk 
# "platform-tools" 
# "build-tools;34.0.0" 
# "platforms;android-34" 
# "cmdline-tools;latest" 
# "cmake;3.10.2.4988404"
# "ndk;23.2.8568313"

