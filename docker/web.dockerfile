FROM rust:1.83

RUN apt-get update -y && apt-get install -y \
  gcc-x86-64-linux-gnux32 \ 
  && rm -rf /var/lib/apt/lists/*

RUN rustup toolchain install nightly \
  && rustup component add rust-src --toolchain nightly \
  && rustup target add wasm32-unknown-emscripten --toolchain nightly

RUN git clone https://github.com/emscripten-core/emsdk.git \
  && cd emsdk \
  && ./emsdk install 3.1.39 \
  && ./emsdk activate 3.1.39