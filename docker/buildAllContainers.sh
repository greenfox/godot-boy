#!/bin/bash
set -e

main()
{
for i in *.dockerfile
do 
    short=${i%.*}
    tag=registry.gitlab.com/greenfox/godot-boy:${short}
    docker build \
    --file $i \
    -t ${tag} -t ${short} \
    .
    built=" ${built} $tag"

done
echo $built
}

cd $(dirname $0)
built=""

main
