FROM rust:1.83

RUN apt-get update -y && apt-get install -y \
  openjdk-17-jdk-headless \
  gcc-aarch64-linux-gnu \
  gcc-x86-64-linux-gnux32 \ 
  mingw-w64 \
  zip \
  && rm -rf /var/lib/apt/lists/*
  
COPY sdk34 /andrdoid_sdk
ENV ANDROID_SDK="/andrdoid_sdk"

RUN rustup target install \
  aarch64-linux-android \
  x86_64-linux-android \
  x86_64-pc-windows-gnu \
  aarch64-unknown-linux-gnu \
  x86_64-unknown-linux-gnu


RUN rustup toolchain install nightly \
  && rustup component add rust-src --toolchain nightly \
  && rustup target add wasm32-unknown-emscripten --toolchain nightly

RUN git clone https://github.com/emscripten-core/emsdk.git \
  && cd emsdk \
  && ./emsdk install 3.1.39 \
  && ./emsdk activate 3.1.39
