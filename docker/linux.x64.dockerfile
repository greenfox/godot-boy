FROM rust:1.83

#linux.x86
RUN apt-get update -y && apt-get install -y \
  gcc-x86-64-linux-gnux32 \ 
  && rm -rf /var/lib/apt/lists/*

RUN rustup target install x86_64-unknown-linux-gnu

