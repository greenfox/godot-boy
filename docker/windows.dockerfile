FROM rust:1.83

#windows
RUN apt-get update -y && apt-get install -y \
  mingw-w64 \
  && rm -rf /var/lib/apt/lists/*
  
RUN rustup target add x86_64-pc-windows-gnu
