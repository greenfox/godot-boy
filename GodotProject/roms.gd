class_name ROM_
extends Node

static var activeRom:ROM = null

@export_file("*.gb","*.gbc") var rom
@export var save_data_path=""

static func create(rom:String,name:String) -> ROM:
	var r = ROM.new()
	r.name = name
	r.rom = rom
	
	r.save_data_path = "user://" + rom.get_file() + ".sav"
	
	return r
